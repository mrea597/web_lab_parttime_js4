"use strict";

/* Your answer here */

//1. make the first div pink
var t = document.getElementsByClassName("text1");
var firstDiv = t[0];
firstDiv.style.backgroundColor = "pink";

//2. make the last paragraph have red text
var lastP = document.getElementById("footer");
lastP.style.color = "red";

//3. hide the second div
var sD = document.getElementsByClassName("text2");
var secDiv = sD[0];
secDiv.style.visibility = "hidden";

//4. change the text in 1st paragraph to say "Hello World"
var fP = document.getElementsByClassName("subtitle");
var firstPar = fP[0];
firstPar.innerText = "Hello World";

//5. add an event handler to the button so that when it’s clicked, all paragraphs become bold
var boldButt = document.getElementById("makeBold");
boldButt.onclick = function() {
    var allPar = document.getElementsByTagName("p");
    for (var i = 0; allPar.length; i++) {
        allPar[i].style.fontWeight = "bold";
    }
}

